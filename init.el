(setq debug-on-error nil)
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(define-key special-event-map [config-changed-event] 'ignore)


(setq my-configuration-path "~/.config/emacs")
(load (concat my-configuration-path "/path-variables.el"))

;; La siguiente line carga tu configuración de otro archivo
(org-babel-load-file
 (expand-file-name (concat my-configuration-path "/emacsconfig.org")))

(load (concat my-configuration-path "/configuration-variables.el"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-electric-math '("$" . "$"))
 '(custom-enabled-themes '(leuven-dark))
 '(package-selected-packages
   '(eglot fireplace trashed js-react-redux-yasnippets emms dashboard org-roam org-contacts org-ref ivy-bibtex apheleia company simple-httpd flymake-eslint auctex telephone-line magit counsel-projectile projectile ivy switch-window yasnippet engine-mode ob-restclient restclient hl-todo speed-type elm-mode nix-mode typescript-mode markdown-mode epresent dockerfile-mode csv-mode ledger-mode haskell-mode yaml-mode emmet-mode crux lorem-ipsum elpher elfeed pdf-tools pandoc indent-guide rainbow-delimiters beacon which-key)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "gray20" :foreground "white smoke" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight regular :height 130 :width normal :foundry "IBM " :family "BlexMono Nerd Font")))))
